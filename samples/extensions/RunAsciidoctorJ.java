///usr/bin/env jbang "$0" "$@" ; exit $?
//JAVA_OPTIONS --add-opens java.base/sun.nio.ch=ALL-UNNAMED --add-opens java.base/java.io=ALL-UNNAMED
//DEPS org.asciidoctor:asciidoctorj:2.5.7
//DEPS org.asciidoctor:asciidoctorj-pdf:2.3.3
import org.asciidoctor.*;
import java.io.File;

public class RunAsciidoctorJ {

    public static void main(String... args) {
        try (Asciidoctor asciidoctor = Asciidoctor.Factory.create()) {
            asciidoctor.convertFile(
                new File(args[0]),
                OptionsBuilder.options()
                    .toFile(true)
                    .backend("html")
                    .safe(SafeMode.UNSAFE)
            );    
        }
    }
}
