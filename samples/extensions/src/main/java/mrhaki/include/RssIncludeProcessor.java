package mrhaki.include;

import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.FeedException;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;
import org.asciidoctor.ast.Document;
import org.asciidoctor.extension.IncludeProcessor;
import org.asciidoctor.extension.PositionalAttributes;
import org.asciidoctor.extension.PreprocessorReader;

import java.io.IOException;
import java.net.URL;
import java.util.Map;

/**
 * IncludeProcessor to include data from RSS feeds.
 * 
 * The following syntax can be used in the markup:
 * include::rss[feed=https://feeds.feedburner.com/mrhaki, entries=10]
 * 
 * The following attriubtes feed and entries are supported.
 * The attribute feed define the RSS feed to include and 
 * the attribute entries defines how many entries from the feed to include. 
 * By default 25 entries are shown.
 */
@PositionalAttributes({"feed", "entries"})
public class RssIncludeProcessor extends IncludeProcessor {
    /**
     * We define here that we support rss as include target.
     * 
     * @param target Value of the target used with the include markup.
     * @return True if target is rss, false otherwise.
     */
    @Override
    public boolean handles(final String target) {
        return "rss".equals(target);
    }

    @Override
    public void process(final Document document, final PreprocessorReader reader,
                        final String target, final Map<String, Object> attributes) {
        // Get value for feed attribute.
        String feed = (String) attributes.get("feed");
        
        // Get value for entries attributes, use 25 as default.
        int entries = Integer.parseInt((String) attributes.getOrDefault("entries", "25"));

        // Get Asciidoc markup content from RSS feed.
        String content = getContentFeed(feed, entries);
        
        // Push our new content into the reader so the markup
        // is replaced with our new Asciidoc markup.
        reader.pushInclude(content, target, feed, 1, attributes);
    }

    private String getContentFeed(final String feed, final int entries) {
        try {
            // We use Rome to read the feed.
            SyndFeed syndFeed = new SyndFeedInput().build(new XmlReader(new URL(feed)));

            // StringBuilder to hold content while loop 
            // through all the entries of the feed.
            StringBuilder content = new StringBuilder();
            
            // Go through the entries, limit based on entries
            // and convert each entry to a Asciidoc list item 
            // with a link.
            syndFeed.getEntries()
                    .stream()
                    .limit(entries)
                    .forEach(entry -> {
                        content.append("* ");
                        content.append(entry.getLink());
                        content.append("[").append(entry.getTitle()).append("]");
                        content.append(System.lineSeparator());
                    });
            
            return content.toString();
        } catch (FeedException | IOException e) {
            return "error getting feed content";
        }
    }
}
