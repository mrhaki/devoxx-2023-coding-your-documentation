package mrhaki.block;

import org.asciidoctor.ast.StructuralNode;
import org.asciidoctor.extension.BlockMacroProcessor;
import org.asciidoctor.extension.Name;
import org.asciidoctor.extension.PositionalAttributes;

import java.util.Map;

/**
 * Example:
 *
 * You can read the following toot:
 *
 * toot::109257559314559536[user=mrhaki,server=mastodon.online]
 */
@Name("toot")
@PositionalAttributes({"user", "server"})
public class TootBlockMacroProcessor extends BlockMacroProcessor {

    private static final String TOOT = """
            <iframe src='https://%s/@%s/%s/embed' class='mastodon-embed' style='max-width: 100%%; border: 0' width='400'  height='250' allowfullscreen='allowfullscreen'></iframe>""";

    @Override
    public Object process(final StructuralNode parent, final String target, final Map<String, Object> attributes) {
        var user = (String) attributes.get("user");
        var server = (String) attributes.get("server");

        var htmlContent = TOOT.formatted(server, user, target);
        return createBlock(parent, "pass", htmlContent);
    }
}
