package mrhaki.block;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.jruby.extension.spi.ExtensionRegistry;

/**
 * ExtensionRegistry implementation to register {@link TootBlockMacroProcessor}.
 */
public class TootBlockMacroExtensionRegistry implements ExtensionRegistry {
    @Override
    public void register(final Asciidoctor asciidoctor) {
        asciidoctor.javaExtensionRegistry().blockMacro(TootBlockMacroProcessor.class);
    }
}
