package mrhaki.post;

import org.asciidoctor.ast.Document;
import org.asciidoctor.extension.Postprocessor;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

/**
 * PostProcessor to add an extra text to footer element in the generated HTML.
 */
public class CustomFooterPostProcessor extends Postprocessor {
    @Override
    public String process(final Document document, final String output) {
        var doc = Jsoup.parse(output, "UTF-8");
        Element footerText = doc.getElementById("footer-text");
        if (footerText != null) {
            footerText.text(footerText.ownText() + " | " + "Enjoy your day!!");
        }

        return doc.html();
    }
}
