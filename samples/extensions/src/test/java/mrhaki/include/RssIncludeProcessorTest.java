package mrhaki.include;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Options;
import org.asciidoctor.SafeMode;
import org.junit.jupiter.api.Test;

import java.io.File;

class RssIncludeProcessorTest {

    @Test
    void includeRssFeed() {
        try (Asciidoctor asciidoctor = Asciidoctor.Factory.create()) {
            // when
            String result = asciidoctor.convertFile(
                    new File("rss.adoc"),
                    Options.builder()
                           .safe(SafeMode.UNSAFE)
                           .build()
            );

            // then
            System.out.println(result);
        }
    }
}
