package mrhaki.pre;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Options;
import org.asciidoctor.SafeMode;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.assertj.core.api.Assertions.assertThat;

class JavaPropsPreprocessorTest {

    @Test
    void javaSysPropsDocumentAttributes() {
        try (Asciidoctor asciidoctor = Asciidoctor.Factory.create()) {
            // given
            asciidoctor.javaExtensionRegistry().preprocessor(JavaPropsPreprocessor.class);
            
            // when
            String result = asciidoctor.convert(
                    "Java version {java-version}",
                    Options.builder().build());
            
            // then
            assertThat(result).contains("Java version 17.0.5");
        }
    }

    @Test
    void javaSysProps() {
        try (Asciidoctor asciidoctor = Asciidoctor.Factory.create()) {
            // given
            asciidoctor.javaExtensionRegistry().preprocessor(JavaPropsPreprocessor.class);

            // when
            asciidoctor.convertFile(
                    new File("javaprops.adoc"),
                    Options.builder()
                           .toFile(true)
                           .safe(SafeMode.UNSAFE)
                           .build());
        }
    }
}
