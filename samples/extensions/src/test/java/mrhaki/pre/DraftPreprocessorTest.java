package mrhaki.pre;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Attributes;
import org.asciidoctor.Options;
import org.asciidoctor.SafeMode;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

class DraftPreprocessorTest {

    @Test
    void renderDraft() {
        
        try (Asciidoctor asciidoctor = Asciidoctor.Factory.create()) {
            // given
            asciidoctor.javaExtensionRegistry().preprocessor(DraftPreprocessor.class);
            
            // when
            asciidoctor.convertFile(
                    new File("draft.adoc"),
                    Options.builder()
                           .toFile(true)
                           .attributes(
                                   Attributes.builder()
                                             .attribute("draft")
                                             .build()
                           )
                           .safe(SafeMode.UNSAFE)
                           .build()
            );
        }
    }
}
