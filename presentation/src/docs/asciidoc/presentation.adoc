= Coding Your Documentation


== @mrhaki@mastodon.online

* https://blog.mrhaki.com/
* https://www.mrhaki.com/
* https://www.jdriven.nl/

== Agenda

[%step]
* Types of documentation
* Docs-as-code
* Asciidoc(tor)(J)
* Asciidoctor(J) extensions
* Publishing

== Types of documentation

[%step]
* Code comments
* README
* Getting started
* Conceptual documentation
* Procedural documentation
* Troubleshooting documentation
* Reference documentation

== Docs-as-code

[%step]
* Documentation close to the code
* Use text format with markup
* Edit in IDE
* Generate markup
* Part of the build pipeline (Maven/Gradle)
* https://doctoolchain.org/docToolchain/v2.0.x/[docToolchain]

== Asciidoc(tor)(J)

[%step]
* Asciidoc markup
* Include files
* Tables
* Document attributes
* Conditions

== Asciidoctor(J) extensions

* Transformation process
[%step]
** Parse source to Abstract Syntax Tree (AST)
** Process AST
** Generate output

== AST

[cols="a,a"]
|===
|
----
= Sample

== Welcome

Welcome to 
Coding Your 
Documentation
----
| 
* `Document` 
** `Section` 
*** `Block` 
**** `Welcome to Coding Your Documentation`
|===

== Extensions (Parse)

[%step]
* Preprocessor
* IncludeProcessor `include::[]`
* BlockProcessor `[block]`
* BlockMacroProcessor `block::[]`
* TreeProcessor

== Extensions (Generation)

[%step]
* InlineMacroProcessor `inline:[]`
* DocinfoProcessor
* PostProcessor

== Samples

* https://github.com/asciidoctor/asciidoctorj-diagram[Asciidoctor Diagram]
* https://github.com/bmuschko/asciidoctorj-tabbed-code-extension[Tabbed Code Blocks]

== Writing your own extension

[%step]
* Extend one of the base classes
* Register extension
* Package

== Publishing

[%step]
* Static site generators
** https://jbake.org[JBake]
** https://docs.antora.org/antora/latest/[Antora]
* https://pandoc.org[Pandoc]
* Confluence

== End

https://gitlab.com/mrhaki/devoxx-2023-coding-your-documentation

image::./qr.png[]